﻿using System;


namespace CQRStest
{
    public class PatientReportRow
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }//class
}
