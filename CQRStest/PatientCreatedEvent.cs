﻿using SimpleCqrs.Eventing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CQRStest
{
    public class PatientCreatedEvent : DomainEvent
    {
        public Guid CommandWithAggregateRootId { get; set; }

    }//class
}
