﻿using SimpleCqrs.Eventing;
using System;
using System.Collections.Generic;
using System.Linq;


namespace CQRStest
{
    public class PatientReportDenormalizer : IHandleDomainEvents<PatientCreatedEvent>, IHandleDomainEvents<PatientNameSetEvent>
    {
        private readonly PatientReportTable accountReportTable;

        public PatientReportDenormalizer(PatientReportTable accountReportTable)
        {
            this.accountReportTable = accountReportTable;
        }

        public void Handle(PatientCreatedEvent domainEvent)
        {
            accountReportTable.Add(new PatientReportRow() { Id = domainEvent.AggregateRootId });
        }

        public void Handle(PatientNameSetEvent domainEvent)
        {
            accountReportTable.Single(p => p.Id == domainEvent.AggregateRootId).Name =
                string.Format("{0} {1}", domainEvent.FristName, domainEvent.LastName);
        }
    }//class
}
