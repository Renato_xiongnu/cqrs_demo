﻿using SimpleCqrs.Commanding;
using System;


namespace CQRStest
{
    public class CreatePatientCommand : ICommand
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }//class
}
