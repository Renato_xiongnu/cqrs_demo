﻿using SimpleCqrs.Commanding;
using SimpleCqrs.Domain;
using System;


namespace CQRStest
{
    public class CreateAccountCommandHandler : CommandHandler<CreatePatientCommand>
    {
        private readonly IDomainRepository domainRepository;

        public CreateAccountCommandHandler(IDomainRepository domainRepository)
        {
            this.domainRepository = domainRepository;
        }

        public override void Handle(CreatePatientCommand command)
        {
            var account = new Patient(Guid.NewGuid());
            account.SetName(command.FirstName, command.LastName);

            domainRepository.Save(account);
        }
    }//class
}
