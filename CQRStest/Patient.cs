﻿using SimpleCqrs.Domain;
using System;


namespace CQRStest
{
    public class Patient : AggregateRoot
    {
        public Patient(Guid id)
        {
            Apply(new PatientCreatedEvent { CommandWithAggregateRootId = id });
        }

        public void OnAccountCreated(PatientCreatedEvent accountCreatedEvent)
        {
            Id = accountCreatedEvent.AggregateRootId;
        }

        internal void SetName(string firstName, string lastName)
        {
            Apply(new PatientNameSetEvent { FristName = firstName, LastName = lastName });
        }
    }//class
}
