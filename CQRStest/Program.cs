﻿using SimpleCqrs.Commanding;


namespace CQRStest
{
    class Program
    {
        static void Main(string[] args)
        {
            var runtime = new SampleRuntime();


            runtime.Start();

            var patientReportTable = new PatientReportTable();
            runtime.ServiceLocator.Register(patientReportTable);

            var command = new CreatePatientCommand { FirstName = "John", LastName = "Doe"};


            var commandBus = runtime.ServiceLocator.Resolve<ICommandBus>();

            commandBus.Send(command);


            runtime.Shutdown();
        }
    }//class


}//ns
