﻿using SimpleCqrs.Eventing;
using System;


namespace CQRStest
{
    public class PatientNameSetEvent : DomainEvent
    {
        public string FristName { get; set; }
        public string LastName { get; set; }
    }//class
}
